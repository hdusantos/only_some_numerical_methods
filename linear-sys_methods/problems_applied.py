# -*- coding: utf-8 -*-
"""
    Exercises: Book: FRANCO (p. 185) 5.1, 5.2, 5.5 e 5.8;
"""

from copy import copy, deepcopy
from jacobi import jacobi_method
from gauss_seidel import gauss_seidel


# QUESTION 5.1
A = [
    [4, -1, 0, -1, 0, 0],
    [-1, 4, -1, 0, -1, 0],
    [0, -1, 4, 0, 0, -1],
    [-1, 0, 0, 4, -1, 0],
    [0, -1, 0, -1, 4, -1],
    [0, 0, -1, 0, -1, 4]]

b = [100, 0, 0, 100, 0, 0]

print("========== QUESTAO 5.1 ==========")
print("Pode ser utilizado o metodo de Jacobi")
jacobi_method(A, b, 0.00001)


# QUESTION 5.5
A = [
    [-4, 1, 0, 1, 0, 0, 0, 0, 0],
    [1, -4, 1, 0, 1, 0, 0, 0, 0],
    [0, 1, -4, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, -4, 1, 0, 1, 0, 0],
    [0, 1, 0, 1, -4, 1, 0, 1, 0],
    [0, 0, 1, 0, 1, -4, 0, 0, 1],
    [0, 0, 0, 0, 1, 0, -4, 1, 0],
    [0, 0, 0, 0, 1, 0, 1, -4, 1],
    [0, 0, 0, 0, 0, 1, 0, 1, -4],
    ]

b = [-50, -50, -150, 0, 0, -100, -50, -50, -150]

print("\n========== QUESTAO 5.5 ==========")
print("Metodo de Gauss-Seidel que garante a convergencia")
gauss_seidel(A, b, 0.00001)
