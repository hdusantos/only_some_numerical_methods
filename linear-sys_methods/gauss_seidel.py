# -*- coding: utf-8 -*-
"""
    This module is an implementation of the Gauss Seidel method
"""


def norm(vector_x):
    """
        Module calculates the norm of a vector
        Args:
            vector_x (list): Vector
        Returns:
            Norm of a vector
    """
    norm_v = sum([x ** 2 for x in vector_x])
    norm_v **= (1/2.0)
    return norm_v


def gauss_seidel(A, b, tolerance=1.0e-05,  v_initial=None):
    """
        Iterative method for solving systems of linear equations.
        Args:
            A (matrix): Coefficient matrix.
            b (list): Vector of constant terms.
            tolerance (float): Tolerance for the stopping criterion
            v_initial (float): Vector Initial
    """
    interation_max = 70
    order = len(A)
    x = v_initial[:] if v_initial != None else [0] * order
    v = [0] * order
    while interation_max:
        for i in range(order):
            addition = 0
            for j in range(i):
                addition = addition + A[i][j] * v[j]
            for j in range(i+1, order):
                addition = addition + A[i][j] * x[j]
            v[i] = (b[i] - addition) / A[i][i]
        if abs(norm(v) - norm(x)) < tolerance:
            break
        else:
            x = v[:]

        interation_max -= 1

    print("Precisao = ", tolerance)
    print("Solucao:")
    
    for i in range(len(x)):
        print("X%d" % (i+1), " = %.8f" % x[i])


def main():
    """
        It receives matrix A, vector b, tolerance and initial vector
    """
    order = int(input("Digite a ordem: "))
    A = range(order)
    A = [range(order) for x in A]

    print("Digite a matriz:")
    for i in range(order):
        A[i] = input()
        A[i] = list(map(float, A[i].split()))
    print("Digite o vetor b:")
    b = input()
    b = list(map(float, b.split()))
    print("Digite o vetor inicial:")
    v_initial = input()
    v_initial = list(map(float, v_initial.split()))
    tolerance = float(input("Digite a precisão:"))

    gauss_seidel(A, b, tolerance, v_initial)

if __name__ == '__main__':
    main()
