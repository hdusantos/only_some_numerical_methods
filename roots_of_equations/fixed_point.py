# -*- coding: utf-8 -*-
"""
    This module is an implementation of fixed point method.
"""
from math import sin, cos, tan, asin, acos, atan, log, exp


def fixed_point(function_x, g_x, x_initial, tolerance):
    """
        This Calculates root Approximation of Functions
        Args:
            function_x (function): Function.
            g_x (function): Function g(x).
            v_initial (float): Value initial
            tolerance (float): Tolerance for the stopping criterion
        Return:
            Solutions of equations
    """
    interation_max = 40
    x = [x_initial, function_x(x_initial)]

    for i in range(interation_max):
        x[1] = g_x(x[0])
        if abs(x[1] - x[0]) < tolerance or abs(function_x(x[1])) < tolerance:
            return x[1]
        x[0] = x[1]


def fixed_point2(function_x,  g_x, x_initial, tolerance):
    """
        Some changes in the previous method
    """
    interation_max = 30
    x = [x_initial, function_x(x_initial)]

    for i in range(interation_max):
        x[1] = g_x(x[0])
        if abs(x[1] - x[0]) < tolerance or abs(function_x(x[1])) < tolerance:
            print("Numero de passos:", (i+1))
            return x[1]
        x[0] = x[1]


def main():
    """
        Receives terminal function and approximates
        using the fixed point method
    """
    fx_input = input("Função(x): ")
    fx = lambda x: eval(fx_input)
    gx_input = input("Função(x): ")
    gx = lambda x: eval(gx_input)
    x0 = float(input("X0: "))
    tolerance = float(input("Precisao: "))
    result = fixed_point(fx, gx, x0, tolerance)
    print("Aproximaçaõ:", result)


if __name__ == '__main__':
    main()
