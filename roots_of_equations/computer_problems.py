# -*- coding: utf-8 -*-
"""
    Exercises
"""

from math import sin, cos, log, exp, acos
from fixed_point import fixed_point, fixed_point2

# Question 1
a = lambda x: x**3 -2*x -2
b = lambda x: exp(x) + x -7
c = lambda x: exp(x) + sin(x) -4

ga = lambda x: (2*x + 2) ** (1/3.0)
gb = lambda x: log(7 - x )
gc = lambda x: log(4 - sin(x))


print("========== QUESTAO 1 ==========")
print("a/ ", fixed_point(a, ga, 1, 0.000000001))
print("b/ ", fixed_point(b, gb, 1, 0.000000001))
print("c/ ", fixed_point(c, gc, 1, 0.000000001))


# Question 2
a = lambda x: (x ** 5) + x -1
b = lambda x: sin(x) - 6*x - 5
c = lambda x: log(x) +x**2 - 3

ga = lambda x: 1 - (x ** 5)
gb = lambda x: (sin(x) + 5) / -6.0
gc = lambda x: exp(x*2 -3)


print("========== QUESTAO 2 ==========")
print("a/ ", fixed_point(a, ga, 0, 0.000000001))
print("b/ ", fixed_point(b, gb, 1, 0.000000001))
print("c/ ", fixed_point(c, gc, 1, 0.000000001))


# Question 4
a = lambda x: (2*x + 2 / float(x**2)) / 3.0
b = lambda x: (2*x + 3 / float(x**2)) / 3.0
c = lambda x: (2*x + 5 / float(x**2)) / 3.0

ga = lambda x: (2*x + 2 / float(x**2)) / 3.0
gb = lambda x: (2*x + 3 / float(x**2)) / 3.0
gc = lambda x: (2*x + 5 / float(x**2)) / 3.0

print("========== QUESTAO 4 ==========")
print("a/ ", fixed_point2(a, ga, 1, 0.000000001))
print("b/ ", fixed_point2(b, gb, 1, 0.000000001))
print("c/ ", fixed_point2(c, gc, 1, 0.000000001))


# Question 5
a = lambda x: (cos(x)**2)
ga = lambda x: cos(x) - cos(x)**1/2.0

print("========== QUESTAO 5 ==========")
print(fixed_point2(a, ga, 0.75, 0.0000001))


# Question 7
a = lambda x: 1 - 5*x + ((15/2.0) * x**2) - ((5/2.0) * x**3)
ga = lambda x: 1 - 5*x + ((15/2.0) * x**2) - ((5/2.0) * x**3)

print("========== QUESTAO 7 ==========")
print("a/ ", fixed_point2(a, ga, 1, 0.000000001))
