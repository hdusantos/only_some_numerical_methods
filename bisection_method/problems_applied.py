# -*- coding: utf-8 -*-
"""
    Exercises: Book SAUER. (p. 30) 1.1 - 1, 2, 4, 5.
"""
from math import sin, cos, log
from bisection_method import bisection_method


function_a = lambda x: (x ** 3) - 9
function_b = lambda x: (3 * (x ** 3)) + (x ** 2) - x - 5
function_c = lambda x: (cos(x) ** 2) - x + 6

result_a = bisection_method(function_a, 1, 3, 0.0000001)
result_b = bisection_method(function_b, 1, 2, 0.0000001)
result_c = bisection_method(function_c, 1, 7, 0.0000001)

# QUESTION 1
print("\n========== QUESTAO 01 ==========")
print("a/ Aproximacao :", result_a[0])
print("b/ Aproximacao :", result_b[0])
print("c/ Aproximacao :", result_c[0])


# QUESTION 2
function_a = lambda x: 6 * x + 5 - sin(x)
function_b = lambda x: log(x) + (x ** 2) - 3

result_a = bisection_method(function_a, -1, 1, 0.000000001)
result_b = bisection_method(function_b, 1, 2, 0.000000001)

print("\n========== QUESTAO 02 ==========")
print("a/ Aproximacao :", result_a[0])
print("b/ Aproximacao :", result_b[0])


# QUESTION 4
function_a = lambda x: (x ** 2) - 2
function_b = lambda x: (x ** 2) - 3
function_c = lambda x: (x ** 2) - 5

result_a = bisection_method(function_a, 1, 2, 0.000000001)
result_b = bisection_method(function_b, 1, 2, 0.000000001)
result_c = bisection_method(function_c, 1, 3, 0.000000001)


print("\n========== QUESTAO 04 ==========")
print("a/")
print("Intervalo Inicial: 1, 2")
print("Aproximacao :", result_a[0])
print("Passos :", result_a[1])

print("b/")
print("Intervalo Inicial: 1, 2")
print("Aproximacao :", result_b[0])
print("Passos :", result_b[1])

print("c/")
print("Intervalo Inicial: 1, 3")
print("Aproximacao :", result_c[0])
print("Passos :", result_c[1])


# QUESTION 5
function_a = lambda x: (x ** 3) - 2
function_b = lambda x: (x ** 3) - 3
function_c = lambda x: (x ** 3) - 5

result_a = bisection_method(function_a, 1, 2, 0.00000001)
result_b = bisection_method(function_b, 1, 2, 0.00000001)
result_c = bisection_method(function_c, 1, 2, 0.00000001)

print("\n========== QUESTAO 05 ==========")
print("a/")
print("Intervalo Inicial: 1, 2")
print("Aproximacao :", result_a[0])
print("Passos :", result_a[1])

print("b/")
print("Intervalo Inicial: 1, 2")
print("Aproximacao :", result_b[0])
print("Passos :", result_b[1])

print("c/")
print("Intervalo Inicial: 1, 2")
print("Aproximacao :", result_c[0])
print("Passos :", result_c[1])
