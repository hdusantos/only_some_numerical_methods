# only_some_numerical_methods
> Implementation of some numerical methods without the use of NumPy
* Bisection method
* Secant method
* False position method
* Fixed point method
* Gauss Seidel method
* Jacobi method
* Cholesky decomposition
* Upper triangular

##### Python version: 3.5.3
