# -*- coding: utf-8 -*-
"""
    This module is an implementation of the secant method and
    false position method
"""
from math import sin, cos, tan, asin, acos, atan, log, exp


def calculate_Xkn(function_x, a, b):
    """
        This function treats division by zero in the calculation of Xk+1
        Args:
            function_x (function): Function to calculate Xk+1
            a (float): k-1
            a (float): k
        Return:
            Xk+1
    """
    try:
        c = function_x(a, b)
    except ZeroDivisionError:
        print("Erro: Divisao por zero, sera retornado o valor da iteracao anterior")
        c = "Erro: Divisao por zero"

    return c


def secant_method(function_x, a, b, tolerance, steps=50):
    """
        This calculate root approximation of functions
        Args:
            function_x (function): Input function
            a (float): Interval 1.
            b (float): Interval 2.
            steps (int): max number of steps
        Return:
            Root Approximation function_x
    """
    xk_plus1 = lambda a, b: b - ((b - a)*function_x(b) / float(function_x(b) - function_x(a)))
    if(function_x(a) * function_x(b)) > 0:
        print("Erro no intervalo")
        return 0.0
    c = 0
    while steps:
        value_xkn = calculate_Xkn(xk_plus1, a, b)
        if isinstance(value_xkn, str):
            break
        if abs(function_x(value_xkn)) <= tolerance:
            break
        c = value_xkn
        a = b
        b = c
        steps -= 1

    return c


def false_position(function_x, a, b, tolerance, steps=50):
    """
        This calculate root approximation of functions
        Args:
            function_x (function): Input function
            a (float): Interval 1.
            b (float): Interval 2.
            steps (int): max number of steps
        Return:
            Root Approximation function_x
    """
    xk_plus1 = lambda a, b: (b * function_x(a) - a * function_x(b)) / float(function_x(a) - function_x(b))
    c = 0
    for i in range(steps):
        value_xkn = calculate_Xkn(xk_plus1, a, b)
        if isinstance(value_xkn, str):
            break
        if abs(function_x(value_xkn)) <= tolerance:
            break
        c = value_xkn
        if function_x(b) == 0:
            break
        elif (function_x(a) * function_x(b)) < 0:
            b = c
        else:
            a = c
    return c

def main():
    """
        Receives terminal function and approximates
        using the Secant and false position method
    """
    fx_input = input("Função(x): ")
    fx = lambda x: eval(fx_input)
    a = float(input("Intervalo A: "))
    b = float(input("Intervalo B: "))
    tolerance = float(input("Precisao: "))
    method = int(input("Metodo: (1)Secante (2)Falsa Posicao: "))
    if method == 1:
        result = secant_method(fx, a, b, tolerance)
    else:
        result = false_position(fx, a, b, tolerance)

    print("Aproximaçaõ:", result)

if __name__ == '__main__':
    main()
