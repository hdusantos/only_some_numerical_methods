# -*- coding: utf-8 -*-
"""
    Exercises: Book SAUER. (p. 66) Secao 1.5 - 1, 4.
"""
from math import sin, exp
from sec_and_fposition import secant_method, false_position

# QUESTION 1 and 2
function_a = lambda x: (x ** 3) - (2 * x) - 2
function_b = lambda x: exp(x) + x - 7
function_c = lambda x: exp(x) + sin(x) - 4

# QUESTION 1
print("\n========== QUESTAO 01 ==========")
print("x0 = 1 e x1 = 2")
print("a/")
print("Aproximacao : %.8f" % secant_method(function_a, 1, 2, 1.0e-20, 6))
print("\nb/")
print("Aproximacao : %.8f" % secant_method(function_b, 1, 2, 1.0e-20, 6))
print("\nc/")
print("Aproximacao : %.8f" % secant_method(function_c, 1, 2, 1.0e-20, 6))

# QUESTION 2
print("\n========== QUESTAO 02 ==========")
print("Aplicado em [1, 2]")
print("a/")
print("Aproximacao : %.8f" % false_position(function_a, 1, 2, 1.0e-20,10))
print("\nb/")
print("Aproximacao : %.8f" % false_position(function_b, 1, 2, 1.0e-20,10))
print("\nc/")
print("Aproximacao : %.8f" % false_position(function_c, 1, 2, 1.0e-20, 10))