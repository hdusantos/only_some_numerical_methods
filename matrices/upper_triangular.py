# -*- coding: utf-8 -*-
"""
    This module is an implementation of the upper triangular
"""


def upper_triangular(A, b):
    """
        Implementation of the method for upper triangular matrix
        Args:
            A (matrix): Matrix
            b (list): Vector
    """
    order = len(A)
    x = [0]*order
    x[order-1] = b[order-1] / float(A[order-1][order-1])
    for i in range(order-1, -1, -1):
        addition = 0.0
        for j in range(i+1, order):
            addition += A[i][j] * x[j]
        x[i] = (b[i] - addition) / float(A[i][i])
    print(x)


def main():
    """
        It receives matrix A, vector b
    """
    order = int(input("Digite a ordem: "))
    A = range(order)
    A = [range(order) for x in A]

    print("Digite a matriz:")
    for i in range(order):
        A[i] = input()
        A[i] = list(map(float, A[i].split()))
    print("Digite o vetor b:")
    b = input()
    b = list(map(float, b.split()))

    upper_triangular(A, b)


if __name__ == '__main__':
    main()